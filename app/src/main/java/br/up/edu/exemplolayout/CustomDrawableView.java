package br.up.edu.exemplolayout;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class CustomDrawableView extends View {
    private ShapeDrawable mDrawable;

    public CustomDrawableView(Context v,AttributeSet as){
        super(v, as);
        drawShape(0);
    }

    public void setTemperature(int tmp){
        drawShape(tmp);
        invalidate(); //this will call onDraw()

    }


    int height=50;
    int y=1000 ;

    public void drawShape(int tmp) {

        if (y > 200) {
            int x = 318;
            int width = 62;
            y = y - tmp;
            height = height + tmp;
            Log.d("temp", "" + height);
            mDrawable = new ShapeDrawable(new RectShape());
            mDrawable.getPaint().setColor(0xFF007FFF);
            mDrawable.setBounds(x, y, x + width, y + height);
        }
    }
    protected void onDraw(Canvas canvas) {
        mDrawable.draw(canvas);
    }
}