package br.up.edu.exemplolayout;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    CustomDrawableView temperatureView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        //drawView = new DrawView(this);
        //drawView.setBackgroundColor(Color.WHITE);
        //setContentView(drawView);

//        SurfaceView surface = (SurfaceView) findViewById(R.id.surface);
//        surface.getHolder().addCallback(new SurfaceHolder.Callback() {
//
//            @Override
//            public void surfaceCreated(SurfaceHolder holder) {
//                // Do some drawing when surface is ready
//                Canvas canvas = holder.lockCanvas();
//                //canvas.drawColor(Color.RED);
//                //holder.setFormat(PixelFormat.TRANSPARENT);
//                int x = canvas.getWidth();
//                int y = canvas.getHeight();
//                int radius;
//                radius = 100;
//                Paint paint = new Paint();
//                paint.setStyle(Paint.Style.FILL);
//                paint.setColor(Color.WHITE);
//                canvas.drawPaint(paint);
//                // Use Color.parseColor to define HTML colors
//                paint.setColor(Color.parseColor("#CD5C5C"));
//                canvas.drawCircle(x / 2, y / 2, radius, paint);
//
//
//                holder.unlockCanvasAndPost(canvas);
//            }
//
//            @Override
//            public void surfaceDestroyed(SurfaceHolder holder) {
//            }
//
//            @Override
//            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
//            }
//        });

    }


    public void setTemp(View v){
        CustomDrawableView temperatureView =
                (CustomDrawableView) findViewById(R.id.custom_drawable_view);
        temperatureView.setTemperature(50);


    }
}